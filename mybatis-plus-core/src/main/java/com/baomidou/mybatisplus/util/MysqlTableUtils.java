package com.baomidou.mybatisplus.util;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.TablesNamesFinder;


/**
 * sql拦截器获取表名称
 * @author yanglei
 * @date 2018/6/20 19:35
 */
public class MysqlTableUtils {

    public static Set<String> getTableNameBySql(String sql) throws JSQLParserException{
        CCJSqlParserManager parser = new CCJSqlParserManager();
        Set<String> list = new HashSet<String>();
        Statement stmt = parser.parse(new StringReader(sql));
        if (stmt instanceof Select) {
            Select selectStatement = (Select) stmt;
            TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
            List tableList = tablesNamesFinder.getTableList(selectStatement);
            for (Iterator iter = tableList.iterator(); iter.hasNext();) {
                String tableName = iter.next().toString();
                list.add(tableName);
            }
        }
        return list;
    }
}
