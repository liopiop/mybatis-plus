package com.baomidou.mybatisplus.plugins;

import com.baomidou.mybatisplus.toolkit.PluginUtils;
import com.baomidou.mybatisplus.util.MysqlTableUtils;
import net.sf.jsqlparser.JSQLParserException;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class, Integer.class }) })
public class TablePrefixInterceptor implements Interceptor {

    private static final Log log = LogFactory.getLog(TablePrefixInterceptor.class);

    private HashMap<String , String > tableLibrary;

    public Object intercept(Invocation invocation) throws Throwable {
        if (tableLibrary == null) {
            return invocation.proceed();
        }
        StatementHandler statementHandler = (StatementHandler)PluginUtils.realTarget(invocation.getTarget());
        MetaObject metaStatementHandler = SystemMetaObject.forObject(statementHandler);
        BoundSql boundSql = (BoundSql) metaStatementHandler.getValue("delegate.boundSql");
        String sql = boundSql.getSql();
        log.debug("原始SQL: " + sql);
        sql = changeSql(sql);
        log.debug("改写后的SQL: " + sql);
        metaStatementHandler.setValue("delegate.boundSql.sql", sql);
        return invocation.proceed();
    }

    public String changeSql(String sql) throws JSQLParserException {
        Set<String> table = MysqlTableUtils.getTableNameBySql(sql);
        for (String tableName : table) {
            if (tableLibrary.containsKey(tableName)) {
                sql = sql.replaceAll(tableName+"\\s", "  `" + tableLibrary.get(tableName) + "`."+tableName+" ");
            }
        }
        return sql;
    }

    public Object plugin(Object target) {
        if (target instanceof StatementHandler) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }

    public void setProperties(Properties properties) {
        if (properties == null) {
            return;
        }
        this.tableLibrary = new HashMap<>();
        for (Object _key : properties.keySet()) {
            String key = ((String) _key);
            String[] values = properties.getProperty(key).split(",");
            for (String v : values) {
                this.tableLibrary.put(v, key);
            }
        }
    }

    private String getPackageByMapper(String mapperId){
        mapperId = mapperId.substring(0, mapperId.lastIndexOf("."));
        mapperId = mapperId.substring(0, mapperId.lastIndexOf("."));
        return mapperId;
    }
}
